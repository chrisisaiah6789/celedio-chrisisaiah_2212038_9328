package pexer3;

import java.io.*;
import java.net.*;


public class ClientExer3 {
    public static void main(String[] args) {
        String fileName = "res/exer3.xml";
        try (
                Socket socket = new Socket("localhost", 2000);
                BufferedReader fileRdr = new BufferedReader(new FileReader(fileName));
                PrintWriter streamWtr = new PrintWriter(socket.getOutputStream(), true);
                BufferedReader streamRdr = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        ) {
            StringBuilder xmlInput = new StringBuilder();
            String line;
            while ((line = fileRdr.readLine()) != null) {
                xmlInput.append(line);
            }
            streamWtr.println(xmlInput.toString()); // send the XML input to the server
            String response;
            while ((response = streamRdr.readLine()) != null) {
                System.out.println(response); // print the response from the server
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
