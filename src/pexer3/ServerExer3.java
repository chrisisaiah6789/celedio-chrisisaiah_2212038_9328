package pexer3;

import java.io.*;
import java.net.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;

public class ServerExer3 {
    public static void main(String[] args) {
        int port = 2000;
        try (
                ServerSocket serverSocket = new ServerSocket(port);
                Socket clientSocket = serverSocket.accept();
                BufferedReader streamRdr = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                PrintWriter streamWtr = new PrintWriter(clientSocket.getOutputStream(), true);
        ) {
            String xmlInput = streamRdr.readLine(); // read the XML input from the client
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlInput);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("citizen");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String name = eElement.getElementsByTagName("name").item(0).getTextContent();
                    int age = Integer.parseInt(eElement.getElementsByTagName("age").item(0).getTextContent());
                    String response;
                    if (age >= 18) {
                        response = name + ", you may exercise your right to vote!";
                    } else {
                        response = name + ", you are still too young to vote!";
                    }
                    streamWtr.println(response); // send the response back to the client
                }
            }
        } catch (IOException e) {
            System.out.println("Error handling client request: " + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
