/**
 * NAME: CELEDIO, CHRIS ISAIAH V.
 * CLASS CODE: 9328
 *
 * EXERCISE 2
 *
 * PROBLEM :
 *
 *      In the same IntelliJ IDEA project, add another package named pexer2. You are to create
 *      ServerExer2 class inside this package. This class should be a modification of the server
 *      program presented in Sample program 1s (SampleServer1) of module 2, but it will be able
 *      to accept as many clients as possible. Make the server to run “continuously” and will
 *      stop executing only if the user will manually stop the server.
 *
 *
 * ALGORITHM :
 *
 *      - Server program starts.
 *      - User runs the telnet client from the command prompt.
 *      - Server asks the name of the user
 *      - User inputs name
 *      - Server asks the age of the user
 *      - User inputs the age
 *      - If user is above 18, the server will tell the user that he/she may have the right to vote
 *        and program loops
 *      - If user is below 18, the server will tell the user that he/she do not have the right to vote
 *        and program loops
 *
 **/

package pexer2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
public class ServerExer2 {

    public static void main(String[] args) {

    int port = 2000; //initiate port number
    while (true) { // loops the server until it gets stopped manually
        try (
                ServerSocket serverSocket = new ServerSocket(port);
                // Creates a server socket with a specified port
                Socket clientSocket = serverSocket.accept();
                // accepts connection to be made to the socket
                BufferedReader streamRdr = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                // Reads text from a character-input stream
                PrintWriter streamWtr = new PrintWriter(clientSocket.getOutputStream(), true);
                // Prints formatted representations of objects to a text-output stream.
        ) {
            while (true) { // loops the program until it gets stopped manually
                // server sends message to client
                streamWtr.println("What is your name? ");
                // server accepts input from client
                String name = streamRdr.readLine();
                int age;
                while (true) {
                    streamWtr.println("What is your age? ");
                    try {
                        age = Integer.parseInt(streamRdr.readLine());
                        if (age <= 0) {
                            throw new NumberFormatException();
                        } else {
                            break;
                        }
                    } catch (NumberFormatException nfe) {
                        // println method will auto flush
                        streamWtr.println("Please enter a valid age.");
                        continue;
                    }
                }
                if (age >= 18) { // if age is above 18, displays message
                    streamWtr.println(name + ", you may exercise your right to vote!");
                } else { // if age is below 18, displays message
                    streamWtr.println(name + ", you are still too young to vote!");
                }
                streamWtr.println("Thank you and good day.");
                streamWtr.println();
                streamWtr.println();
                streamWtr.println();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

}
