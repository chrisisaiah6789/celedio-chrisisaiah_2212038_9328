 /**
 * NAME: CELEDIO, CHRIS ISAIAH V.
 * CLASS CODE: 9328
 *
 * EXERCISE 1: Internet Addressing with Java
 *
 * PROBLEM : Create a program that will look up a variable number of hostnames/IP addresses
 *      - The program will ask the name of a host to search and
 *      then immediately display the hostnames and IP addresses
 *      of the host given by the user
 *      - The program will then ask the user if he/she wants to
 *      search for another host (if yes, repeat the process above,
 *      otherwise, exit the program)
 *
 * ALGORITHM :
 *      - Program starts and displays a message to input the hostname
 *      - User inputs the hostname
 *      - The program displays the number of hosts/IPS
 *      - The program displays the host name and its corresponding IP addresses
 *      - The program asks the user to search for another hostname
 *      - If the user chooses to search for another hostname, host counter increments and the program loops
 *      = If the user chooses not to search for another hostname, the program displays a farewell message and closes the program.
 *
 **/

package pexer1;

import java.net.InetAddress;
import java.util.Scanner;

public class PreExercise1 {


    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); // Scanner method for user input
        boolean tryAgain = true;
        int hostNo = 1; // initial count for the counter
        while (tryAgain) { // loops if tryAgain is true
            System.out.print("HOST " + hostNo + " - Type Hostname: "); // prints the counter for each host search
            String hostname = input.nextLine(); // let users input the desired hostname
            try {
                InetAddress[] address = InetAddress.getAllByName(hostname); // returns an array of IP addresses for the specified hostname
                System.out.println("Number of Hosts/IPs: " + address.length); // displays the number of hosts or IPs found
                System.out.println("%d%n, Host name %d%n, IP Address ");
                for (InetAddress addresses : address) {
                    System.out.println(hostname + "     " + addresses.getHostAddress()); // displays the list of host names and IP addresses
                }
            } catch (Exception e) {
                System.out.println(e);
            }
            System.out.print("Search another [y/n]? "); // asks user to search for another hostname
            String response = input.nextLine();
            if (response.equalsIgnoreCase("n")) { // when input is "n", stops the loop and closes the program
                tryAgain = false;
                System.out.println("Thank you for using the program!");
                System.out.println("Exiting program...");
            }
            hostNo++; //increments the host counter
        }
        input.close(); //closes scanner input
    }
}
